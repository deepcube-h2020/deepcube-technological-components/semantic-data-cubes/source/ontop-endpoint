FROM ontop/ontop-endpoint
WORKDIR /opt/ontop
RUN wget -q https://jdbc.postgresql.org/download/postgresql-42.4.0.jar
RUN mkdir jdbc
RUN mkdir input
RUN mv postgresql-42.4.0.jar jdbc/
RUN wget -q https://gitlab.com/SLIArtur/deepcube_fdw/-/raw/main/Ontologies/UC3_Fire/UC3_Fire-Ontology.owl?inline=false -O input/UC3-Ontology.owl
RUN wget -q https://gitlab.com/SLIArtur/deepcube_fdw/-/raw/main/Ontologies/UC3_Fire/UC3_Fire-Ontology.obda?inline=false -O input/UC3-Ontology.obda
COPY UC3-Ontology.properties.sample input/UC3-Ontology.properties.sample
EXPOSE 8080
ENTRYPOINT ["/opt/ontop/entrypoint.sh"]